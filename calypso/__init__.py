# -*- coding: utf-8 -*-
#
# This file is part of Calypso - CalDAV/CardDAV/WebDAV Server
# Copyright © 2011 Keith Packard
# Copyright © 2008-2011 Guillaume Ayoub
# Copyright © 2008 Nicolas Kandel
# Copyright © 2008 Pascal Halter
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Calypso.  If not, see <http://www.gnu.org/licenses/>.

"""
Calypso Server module.

This module offers 4 useful classes:

- ``HTTPServer`` is a simple HTTP server;
- ``HTTPSServer`` is a HTTPS server, wrapping the HTTP server in a socket
  managing SSL connections;
- ``CollectionHTTPHandler`` is a WebDAV request handler for HTTP(S) servers.
- ``CalypsoApp`` is a WSGI-style HTTP request handler

To use this module, you should take a look at the file ``calypso.py`` that
should have been included in this package.

"""

from cStringIO import StringIO
import os
import os.path
import base64
import socket
import time
import email.utils
import logging
import rfc822
import ssl
import functools

# Manage Python2/3 different modules
# pylint: disable=F0401
try:
    from http import client, server
except ImportError:
    import httplib as client
    import BaseHTTPServer as server
# pylint: enable=F0401

from . import acl, config, webdav, xmlutils, paths, gssapi, principal

log = logging.getLogger()
ch = logging.StreamHandler()
formatter = logging.Formatter("%(message)s")
ch.setFormatter (formatter)
log.addHandler(ch)
negotiate = gssapi.Negotiate(log)

VERSION = "1.5"

def _check(request, function, environ, start_response):
    """Check if user has sufficient rights for performing ``request``."""
    # ``_check`` decorator can access ``request`` protected functions
    # pylint: disable=W0212
    owner = user = password = None
    negotiate_success = False

    resource = identify_resource(environ['PATH_INFO'])
    collection = collection_singleton(environ['PATH_INFO'])
    entity = resource or collection or None

    if "REMOTE_USER" in environ:
        user = environ["REMOTE_USER"]

    authorization = environ.get("HTTP_AUTHORIZATION", None)
    if authorization:
        if authorization.startswith("Basic"):
            challenge = authorization.lstrip("Basic").strip().encode("ascii")
            plain = request._decode(environ.get('CONTENT_TYPE', ''), base64.b64decode(challenge))
            user, password = plain.split(":")
        elif negotiate.enabled():
            user, negotiate_success = negotiate.try_aaa(authorization, request, entity)

    client_info = dict([
        (name, environ.get(name, None)) for name in
            ("HTTP_USER_AGENT", "HTTP_X_CLIENT", "HTTP_ORIGIN")
        if name in environ])

    # bound privilege checker that can be used by principals etc in discovery too
    has_right = functools.partial(request.acl.has_right, user=user, password=password)

    # Also send UNAUTHORIZED if there's no collection. Otherwise one
    # could probe the server for (non-)existing collections.
    if has_right(entity) or negotiate_success:
        return function(request, context={
            "user": user,
            "client_info": client_info,
            "has_right": has_right},
            environ=environ, start_response=start_response,
            )
    else:
        if negotiate.enabled():
            start_response('401 Unauthorized', [
                ('WWW-Authenticate', 'Negotiate')])
        else:
            start_response('401 Unauthorized', [
                ("WWW-Authenticate",
                'Basic realm="Calypso CalDAV/CardDAV server - password required"')])
    # pylint: enable=W0212


class HTTPServer(server.HTTPServer):
    """HTTP server."""
    PROTOCOL = "http"

    # Maybe a Pylint bug, ``__init__`` calls ``server.HTTPServer.__init__``
    # pylint: disable=W0231
    def __init__(self, address, handler):
        """Create server."""
        server.HTTPServer.__init__(self, address, handler)
    # pylint: enable=W0231


class HTTPSServer(HTTPServer):
    """HTTPS server."""
    PROTOCOL = "https"

    def __init__(self, address, handler):
        """Create server by wrapping HTTP socket in an SSL socket."""
        HTTPServer.__init__(self, address, handler)
        self.socket = ssl.wrap_socket(
            socket.socket(self.address_family, self.socket_type),
            server_side=True,
            certfile=os.path.expanduser(config.get("server", "certificate")),
            keyfile=os.path.expanduser(config.get("server", "key")),
            ssl_version=ssl.PROTOCOL_SSLv23)
        self.server_bind()
        self.server_activate()

def collection_singleton(p):
    path = paths.collection_from_path(p)
    if not path:
        return None
    if not path in CollectionHTTPHandler.collections:
        CollectionHTTPHandler.collections[path] = webdav.Collection(path)
    return CollectionHTTPHandler.collections[path]

def identify_resource(path):
    """Return a Resource object corresponding to the path (this is used for
    everything that is not a collection, like Principal and HomeSet objects)"""

    if path in ("/.well-known/carddav", "/.well-known/caldav"):
        return principal.WellKnownDav()

    try:
        left, right = config.get('server', 'user_principal').split('%(user)s')
    except ValueError:
        raise ValueError("user_principal setting must contain %(user)s.")

    if not path.startswith(left):
        return None

    remainder = path[len(left):]
    if right not in remainder:
        return None

    username = remainder[:remainder.index(right)]
    remainder = remainder[remainder.index(right)+len(right):]

    if remainder == principal.AddressbookHomeSet.type_dependent_suffix + "/":
        return principal.AddressbookHomeSet(username)
    elif remainder == principal.CalendarHomeSet.type_dependent_suffix + "/":
        return principal.CalendarHomeSet(username)
    elif remainder == "":
        return principal.Principal(username)
    else:
        return None

def collection_singleton(p):
    path = paths.collection_from_path(p)
    if not path:
        return None
    if not path in CalypsoApp.collections:
        CalypsoApp.collections[path] = webdav.Collection(path)
    return CalypsoApp.collections[path]


class CalypsoApp(object):

    _encoding = config.get("encoding", "request")

    # Decorator checking rights before performing request
    check_rights = lambda function: lambda request, environ, start_response: _check(request, function, environ, start_response)

    def __init__(self):
        self.acl = acl.load()

    def __call__(self, environ, start_response):
        mname = 'do_' + environ['REQUEST_METHOD']
        if not hasattr(self, mname):
            log.error("Unsupported method (%r)", self.command)
            start_response('501 Unknown method', [])
            return ["Unsupported method (%r)" % self.command]
        method = getattr(self, mname)
        return method(environ, start_response)

    collections = {}

    def _decode(self, content_type, text):
        """Try to decode text according to various parameters."""
        # List of charsets to try
        charsets = []

        # First append content charset given in the request
        if content_type and "charset=" in content_type:
            charsets.append(content_type.split("charset=")[1].strip())
        # Then append default Calypso charset
        charsets.append(self._encoding)
        # Then append various fallbacks
        charsets.append("utf-8")
        charsets.append("iso8859-1")

        # Try to decode
        for charset in charsets:
            try:
                return text.decode(charset)
            except UnicodeDecodeError:
                pass
        raise UnicodeDecodeError

    # Naming methods ``do_*`` is OK here
    # pylint: disable=C0103

    @check_rights
    def do_GET(self, context, environ, start_response):
        """Manage GET request."""
        return self.do_get_head(context, True, environ, start_response)

    @check_rights
    def do_HEAD(self, context, environ, start_response):
        """Manage HEAD request."""
        return self.do_get_head(context, False, environ, start_response)

    def do_get_head(self, context, is_get, environ, start_response):
        """Manage either GET or HEAD request."""

        path = environ['PATH_INFO']
        answer_text = ''
        try:
            item_name = paths.resource_from_path(path)
            collection = collection_singleton(path)
            resource = identify_resource(path)
            if item_name and collection:
                # Get collection item
                item = collection.get_item(item_name)
                if item:
                    if is_get:
                        answer_text = item.text
                    etag = item.etag
                else:
                    start_response('410 Gone', [('Content-Length', '0')])
                    return []
            elif collection:
                # Get whole collection
                if is_get:
                    answer_text = collection.text
                etag = collection.etag
            elif resource:
                return resource.do_get_head(context, is_get, environ, start_response)
            else:
                start_response('404 Not Found', [])
                return []

            if is_get:
                try:
                    answer = answer_text.encode(self._encoding, "xmlcharrefreplace")
                except UnicodeDecodeError:
                    answer_text = answer_text.decode(errors="ignore")
                    answer = answer_text.encode(self._encoding,"ignore")
            else:
                answer = ''

            start_response('200 OK', [
                ('Content-Length', str(len(answer))),
                ("Content-Type", "text/calendar"),
                ("Last-Modified", email.utils.formatdate(time.mktime(collection.last_modified))),
                ("ETag", etag)])
            if is_get:
                return [answer]
            return []
        except Exception:
            log.exception("Failed HEAD for %s", path)
            start_response('400 Bad Request', [])
            return []

    def if_match(self, environ, item):
        header = environ.get("HTTP_IF_MATCH", item.etag)
        header = rfc822.unquote(header)
        if header == item.etag:
            return True
        quoted = '"' + item.etag + '"'
        if header == quoted:
            return True
        extraquoted = rfc822.quote(quoted)
        if header == extraquoted:
            return True
        return False

    @check_rights
    def do_DELETE(self, context, environ, start_response):
        """Manage DELETE request."""
        try:
            path = environ['PATH_INFO']
            item_name = paths.resource_from_path(path)
            collection = collection_singleton(path)
            item = collection.get_item(item_name)

            if item and self.if_match(environ, item):
                # No ETag precondition or precondition verified, delete item
                answer = xmlutils.delete(path, collection, context=context)

                start_response('200 OK', [
                    ('Content-Length', str(len(answer))),
                    ('Content-Type', 'text/xml')])
                return [answer]
            elif not item:
                # Item does not exist
                start_response('404 Not Found', [])
                return []
            else:
                # No item or ETag precondition not verified, do not delete item
                start_response('412 Precondition Failed', [])
                return []
        except Exception:
            log.exception("Failed DELETE for %s", path)
            start_response('400 Bad Request', [])
            return []

    @check_rights
    def do_MKCALENDAR(self, context, environ, start_response):
        """Manage MKCALENDAR request."""
        start_response('201 Created', [])
        return []

    def do_OPTIONS(self, environ, start_response):
        """Manage OPTIONS request."""
        start_response('204 No Content', [
            ("Allow", "DELETE, HEAD, GET, MKCALENDAR, "
                      "OPTIONS, PROPFIND, PUT, REPORT"),
            ("DAV", "1, access-control, calendar-access, addressbook")])
        return []

    @check_rights
    def do_PROPFIND(self, context, environ, start_response):
        """Manage PROPFIND request."""
        try:
            path = environ['PATH_INFO']
            xml_request = environ['wsgi.input'].read(int(environ.get('CONTENT_LENGTH', '0')))
            log.debug("PROPFIND %s", xml_request)
            answer = xmlutils.propfind(
                path, xml_request, collection_singleton(path), identify_resource(path),
                environ.get("HTTP_DEPTH", "infinity"),
                context)
            log.debug("PROPFIND ANSWER %s", answer)

            start_response('207 Multi-Status', [
                ('Content-Length', str(len(answer))),
                ("DAV", "1, calendar-access"),
                ("Content-Type", "text/xml")])

            return [answer]
        except Exception:
            log.exception("Failed PROPFIND for %s", path)
            start_response('400 Bad Request', [])
            return []

    @check_rights
    def do_SEARCH(self, context, environ, start_response):
        """Manage SEARCH request."""
        try:
            path = environ['PATH_INFO']
            start_response('204 No Content', [])
            return []
        except Exception:
            log.exception("Failed SEARCH for %s", path)
            start_response('400 Bad Request', [])
            return []

    @check_rights
    def do_PUT(self, context, environ, start_response):
        """Manage PUT request."""
        try:
            path = environ['PATH_INFO']
            item_name = paths.resource_from_path(path)
            collection = collection_singleton(path)
            item = collection.get_item(item_name)
            if not item or self.if_match(environ, item):

                # PUT allowed in 3 cases
                # Case 1: No item and no ETag precondition: Add new item
                # Case 2: Item and ETag precondition verified: Modify item
                # Case 3: Item and no Etag precondition: Force modifying item
                content_type = environ.get("CONTENT_TYPE", None)
                webdav_request = self._decode(content_type, self.xml_request)
                new_item = xmlutils.put(path, webdav_request, collection, context=context)

                log.debug("item_name %s new_name %s", item_name, new_item.name)
                etag = new_item.etag
                #log.debug("replacement etag %s", etag)

                start_response('201 Created', [
                    ("ETag", etag),
                    ])
                return []
            else:
                #log.debug("Precondition failed")
                # PUT rejected in all other cases
                start_response('412 Precondition Failed', [])
                return []
        except Exception:
            log.exception('Failed PUT for %s', path)
            start_response('400 Bad Request', [])
            return []

    @check_rights
    def do_REPORT(self, context, environ, start_response):
        """Manage REPORT request."""
        try:
            path = environ['PATH_INFO']
            xml_request = environ['wsgi.input'].read(int(environ['CONTENT_LENGTH']))
            log.debug("REPORT %s %s", path, xml_request)
            collection = collection_singleton(path)
            answer = xmlutils.report(path, xml_request, collection)
            log.debug("REPORT ANSWER %s", answer)
            start_response('207 Multi-Status', [
                ('Content-Length', str(len(answer))),
                ("Content-Type", "text/xml"),
                ])
            return [answer]
        except Exception:
            log.exception("Failed REPORT for %s", path)
            start_response('400 Bad Request', [])
            return []

    # pylint: enable=C0103


class CollectionHTTPHandler(server.BaseHTTPRequestHandler):
    """HTTP requests handler for WebDAV collections."""

    # We do set Content-Length on all replies, so we can use HTTP/1.1
    # with multiple requests (as desired by the android CalDAV sync program

    app = CalypsoApp()

    protocol_version = 'HTTP/1.1'

    timeout = 90

    server_version = "Calypso/%s" % VERSION
 
    def address_string(self):
        return str(self.client_address[0])

    def send_connection_header(self):
        conntype = "Close"
        if self.close_connection == 0:
            conntype = "Keep-Alive"
        self.send_header("Connection", conntype)

    def send_calypso_response(self, response, length):
        self.send_response(response)
        self.send_connection_header()
        self.send_header("Content-Length", str(length))
        for header, value in config.items('headers'):
            self.send_header(header, value)

    def handle_one_request(self):
        """Handle a single HTTP request.

        You normally don't need to override this method; see the class
        __doc__ string for information on how to handle specific HTTP
        commands such as GET and POST.

        """
        try:
            self.close_connection = 1
            self.wfile.flush()

            self.connection.settimeout(5)

            self.raw_requestline = self.rfile.readline(65537)

            self.connection.settimeout(90)

            if len(self.raw_requestline) > 65536:
                log.error("Read request too long")
                self.requestline = ''
                self.request_version = ''
                self.command = ''
                self.send_error(414)
                return
            if not self.raw_requestline:
                log.error("Connection closed")
                return
            log.debug("First line '%s'", self.raw_requestline)
            if not self.parse_request():
                # An error code has been sent, just exit
                self.close_connection = 1
                return
            # parse_request clears close_connection on all http/1.1 links
            # it should only do this if a keep-alive header is seen
            self.close_connection = 1
            conntype = self.headers.get('Connection', "")
            if (conntype.lower() == 'keep-alive'
                and self.protocol_version >= "HTTP/1.1"):
                log.debug("keep-alive")
                self.close_connection = 0
            reqlen = self.headers.get('Content-Length', "0")
            log.debug("reqlen %s", reqlen)
            self.xml_request = self.rfile.read(int(reqlen))
            environ = {
                'REQUEST_METHOD': self.command,
                'CONTENT_LENGTH': reqlen,
                'PATH_INFO': self.path,
                'wsgi.input': StringIO(self.xml_request)
            }
            for name, value in self.headers.items():
                environ['HTTP_' + name.replace('-', '_').upper()] = value
            def start_response(status, headers):
                (code, message) = status.split(' ', 1)
                self.send_response(int(code), message)
                self.send_connection_header()
                for header, value in headers:
                    self.send_header(header, value)
                self.end_headers()
            lines = self.app(environ, start_response)
            if lines is not None:
                self.wfile.writelines(lines)
            self.wfile.flush() #actually send the response if not already done.
        except socket.timeout as e:
            #a read or a write timed out.  Discard this connection
            log.error("Request timed out: %r", e)
            self.close_connection = 1
            return
        except ssl.SSLError, x:
            #an io error. Discard this connection
            log.error("SSL request error: %r", x.args[0])
            self.close_connection = 1
            return
